(ns re-frame-progress-bar.events
  (:require
   [re-frame.core :as re-frame]
   [re-frame-progress-bar.db :as db]))


(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   {:actual 1 :expected 39}))

(re-frame/reg-event-db
 ::set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(re-frame/reg-event-db
  ::inc-actual
  (fn [db [_ x]]
    (let [actual-db (:actual db)
          expected-db (:expected db)
          new (+ x actual-db)]
      (if (>= new expected-db)
        (assoc db :actual expected-db)
        (assoc db :actual new)))))

(re-frame/reg-event-db
  ::reset-expected
  (fn [db [_ expected]]
    (assoc db :expected expected :actual 1)))