(ns re-frame-progress-bar.tutorial
  (:require
    [reagent.core :as reagent]
    [re-frame.core :as rf]
    [re-frame-progress-bar.subs :as subs]
    [re-frame-progress-bar.events :as events]))

(defn progress [done total]
  (let [s (reagent/atom {})]
    (fn [done total]
      (let [percent (str (.toFixed (* 100 (/ done total)) 1) "%")]
        [:div {:style {:position :relative
                       :overflow :hidden
                       :background-color "#efe"
                       :border-radius 100
                       :line-height "1.3em"}}
         [:div {:style {:position         :absolute
                        :top              0
                        :bottom           0
                        :border-radius 100
                        :width            percent
                        :transition       "width 0.1s"
                        :background-color :green}}]
         [:div {:style {:position   :relative
                        :text-align :left}}
          [:span {:style {:margin-left (:left @s)
                          :color       :white}}
           percent]]
         [:div {:style {:text-align :center}}
          [:span
           {:ref #(if %
                    (swap! s assoc :left (.-offsetLeft %))
                    (swap! s assoc :left 0))}
           percent]]]))))

(rf/dispatch [::events/reset-expected 39])

(defonce _interval (js/setInterval
                     #(rf/dispatch [::events/inc-actual 2.3])
                     1000))

(defn ui []
  (let [actual @(rf/subscribe [::subs/actual])
        expected @(rf/subscribe [::subs/expected])]
    [:div
     [progress actual expected]]))